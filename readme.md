# doticon

## Installation

```bash
curl https://gitlab.com/youtopia.earth/dev/doticon/raw/master/install | bash
```

## Usage

```bash
cp custom-icon-form-my-folder.png /path/to/folder/.icon
cd /path/to/folder
doticon
```

## Simple command cheat (without needing doticon)
```bash
cp custom-icon-form-my-folder.png /path/to/folder/.icon
cd /path/to/folder
gio set . metadata::custom-icon .icon
```

## License
[MIT](https://choosealicense.com/licenses/mit/)